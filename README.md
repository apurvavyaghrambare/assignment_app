Index
1. Introduction
2. RDBMS
3. permit to user
4. Data types in databases
5. Primary key and foreign key
6. Create Table, Insert user and fetch
7. Use of foreigh key
8. Add and remove primary and foreign key
9. Add and remove unique constraint
10. Change column name
11. Change column datatype
12. Practice Questions
13. Insert data in tables
14. Update data in table
15. Deleting data from tables
16. SELECT, WHERE(AND,OR),Inequality symbols,NULL VALUES,IN, NOT IN,BETWEEN,LIKE,ORDER BY, DISTINCT,LIMIT, COLUMN NAME ALIAS
17. JOIN, INNER JOINS, LEFT JOIN, RIGHT JOIN, 
18. Normalization, 1NF, 2NF, 3NF
19. Relationship, 1 to 1, 1 to many, many to many, Constraints
20. Create films table
21. 












1.Introduction

SELECT * FROM Customers;
Database - organised collection of information(data). eg. book

Allow us to access and interact with data, thats where DBMS and SQL comes into play.

SQL:-

stands for structured query language. language is used to communicate to database. It is used to perform tasks on database.

MYSQL:-

It is RDBMS. It provides a UI to interact and access with the database. RDBMS include:

Mysql

PostgerSQL

SQL Server

Oracle

In this we perform sql query in mysql UI and then it interacts with database to access data.
--------------------------------------------------------------------------------------------------------------------------------------------

2. RDBMS:

relational database is a collection of data organized into tables.

Tables contains columns, rows with particular instances of that data category.

RDBMS is what we use to access and interact with the relational database.
--------------------------------------------------------------------------------------------------------------------------------------------

3. user and priviledges>user account>Administrative roles> Roles- description

This is how we give permit to account user.

SHOW DATABASES; 
will show all the databases present.

CREATE DATABASE test;
will create a new database

USE test;
To go inside the test datbase.

SHOW TABLES;
will show all the tables present in test database.

DROP DATABASE test;
will delete the test database.

ALTER TABLE nsjds;                                                  
will make changes in table

ADD COLUMN jfjdncn VARCHAR(50)
Add new column in existing table.

TRUNCATE TABLE hello;
will delete the data of table

DESCRIBE addresses;
will describe the table.

--------------------------------------------------------------------------------------------------------------------------------------------

4.Data types in databases:

INT = whole numbers

FLOAT(M,D) = decimal numbers(Approx)

DECIMAL(M,D) = Decimal number (precise)

CHAR(N) = Fixed length character

VARCHAR(N) = varying length character.

ENUM('M','F') = Value from a defined list

Boolean = True or false values.

DATE = Date(YYYY-MM-DD)

DATETIME = Date and the time (YYYY-MM-DD HH-MM-SS)

TIME = Time(HHH-MM-SS)

YEAR = Year(YYYY)
--------------------------------------------------------------------------------------------------------------------------------------------

5. Primary key and foreign key:

Primery Key:
A primery key is a column ,or set of columns, which uniquely identifies a record in a table.
A primary key must be unique.
A primary key cant be NULL.
A table can have only one primary key.

Foreign Key:
Its used to link two tables together.
FK is a column whose values match with the values of another tables primary key column.
table with primary key is called reference or parent table, table with the foreign key is called the Child table.
A table can have multiple foreign keys.

--------------------------------------------------------------------------------------------------------------------------------------------

6. CREATE TABLE hello(
   id INT AUTO_INCREMENT PRIMARY KEY,      -it will auto inc the primary key.
   name VARCHAR(30),                       -its a name column with limit of 30
   price DECIMAL(3,2)                      -its decimal value upto 2,3 decimal places. 
);

SHOW TABLES;
--------------------------------------------------------------------------------------------------------------------------------------------

Insert user:

INSERT INTO EMPLOYEE VALUES (0001, 'Clark', 'Sales');
INSERT INTO EMPLOYEE VALUES (0002, 'Dave', 'Accounting');
INSERT INTO EMPLOYEE VALUES (0003, 'Ava', 'Sales');

-- fetch 
SELECT * FROM EMPLOYEE WHERE dept = 'Sales';
--------------------------------------------------------------------------------------------------------------------------------------------

7. Use of foreigh key:
(
   name VARCHAR(30),                       
   price DECIMAL(3,2)
   FOREIGN KEY(product_id) REFERENCES products(id),          -FOREIGN KEY(column) REFERENCES table_name(column)   -syntax
);
--------------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE products                                                  - will make changes in table
ADD COLUMN no_means VARCHAR(50);                                                 - Add new column in existing table.
DROP COLUMN no_means                                                  - will delete column
DROP TABLE hello;                                                     -will delete the table
TRUNCATE TABLE hello;                                                 - will delete the data of table

--------------------------------------------------------------------------------------------------------------------------------------------
8. Add and remove primary and foreign key

ALTER TABLE addresses;
ADD PRIMARY KEY(id);                                                     - will add another Primary key in table.

ALTER TABLE addresses;
DROP PRIMARY KEY;                                                     - will delete Primary key in table.

ALTER TABLE <tablename>
ADD CONSTRAINT<constraintname>                                        - constraint name means the new name to the old column
FOREIGN KEY(<column>) REFERENCES <table_name>(<primary key column name>)               - will add new foreign key.

ALTER TABLE people
DROP FOREIGN KEY fk_address                                            - will delete the added Foreign key.
--------------------------------------------------------------------------------------------------------------------------------------------
9. Add and remove unique constraint

ALTER TABLE <tablename>
ADD CONSTRAINT<constraint name>UNIQUE(<columnname>);                     -Add unique constraint

ALTER TABLE <tablename>
DROP INDEX<constraintname>;                                              -remove unique constraint
--------------------------------------------------------------------------------------------------------------------------------------------
10.Change column name
USE test;
ALTER TABLE<tablename>CHANGE'old_column_name''new_column_name'<data type>;            -Change column name      datatype = VARCHAR(20)
SELECT * FROM pets;
--------------------------------------------------------------------------------------------------------------------------------------------

11. Change column datatype
USE test;
ALTER TABLE<tablename>MODIFY<columnname><datatype>;                                   -Change column datatype      datatype = VARCHAR(20)
DESCRIBE addresses;
--------------------------------------------------------------------------------------------------------------------------------------------
12. Practice Questions

Add primary key to the id fields in the pets and people table?

ALTER TABLE people;
ADD PRIMARY KEY(id);

Add a foreign key to the owner_id field in the pets table referencing the id field in the people table?

ALTER TABLE pets;
ADD CONSTRAINT aqua_id;
ADD FOREIGN KEY owner_id REFERENCING people(id);

Add a column named email to the people table?

ALTER TABLE people;
ADD COLUMN email VARCHAR(20);

Add a unique constraint to the email column in the people table?

ALTER TABLE people;
ADD CONSTRAINT yup_id UNIQUE(email); 
DESCRIBE people;

Rename the name column in the pets table to 'first_name'?

USE test;
ALTER TABLE pets CHANGE'names''first_name'VARCHAR(30);
DESCRIBE pets;

Change the postcode datatype to CHAR(7) in the addresses table

ALTER TABLE addresses MODIFY postcode CHAR(7);
DESCRIBE addresses;
--------------------------------------------------------------------------------------------------------------------------------------------
13. Insert data in tables.

INSERT INTO <tablename> (<column1>,<column2>,<column3>);
VALUEs('value1','value2','value3');

eg. 
INSERT INTO products(name,prize,place)
VALUES('machiato',3.00,'brazil'),('costa',5.00,'honolulu'),('latte',9.50,'india');
--------------------------------------------------------------------------------------------------------------------------------------------
14. Update data in table

USE coffee_store;

SELECT * FROM products;

UPDATE<tablename>
SET <columnname> = 'value'
WHERE <column name> = 'value';

eg.
UPDATE products
SET coffee_origin = 'Sri lanka'
WHERE id = 7;

UPDATE products
SET coffee_origin = 'Sri lanka',prize = '3.25'
WHERE name = 'Americanno';
--------------------------------------------------------------------------------------------------------------------------------------------
15. Deleting data from tables

DELETE FROM <table_name>
WHERE name = 'John';

DELETE FROM <table_name>
WHERE gender = 'F';                                                    - will delete more than one row.
--------------------------------------------------------------------------------------------------------------------------------------------
16. command

SELECT command
SELECT last_name,Phone_number FROM people;
---------------------------------------------------------

WHERE CLAUSE (AND,OR)
SELECT * FROM products
WHERE coffee_orrigin = 'columbia';
AND last_name = 'sharma'
OR Phone_number = '393664'
---------------------------------------------------------

Inequality symbols:
>    greater than
>=   greater than equal to
<    less than
<=   lessthan equal to
*/

eg.
SELECT * FROM products
WHERE prize >= 3.00;
----------------------------------------------------------

NULL VALUES

SELECT * FROM customers
WHERE phone_number IS NULL;

SELECT * FROM customers
WHERE phone_number IS NOT NULL;

Q. From the customer table, select the first name and phone numbers of all the females who have a last name of Bluth?

Ans.
SELECT first_name,phone_number From customer
WHERE gender = 'F'
AND last_name = 'Blith';

Q. From a products table, select the name of all the products that have a prize greater than 3.00 or a coffee origin of srilanka

SELECT * FROM products
WHERE prize > 3.00
OR coffee_origin = 'srilanka';

Q. How many male customers dont have a phone number entered into the customers table

Ans.
SELECT * FROM customers
WHERE gender = 'M'
AND phone_number IS NULL;
---------------------------------------------------------
IN, NOT IN: 

SELECT * FROM customers
WHERE last_name IN('Taylor','Blutch','Armstrong');

SELECT * FROM customers
WHERE fisrt_name NOT IN('katie','John'.'George');
---------------------------------------------------------
BETWEEN:

SELECT product_id,customer_id,order_time FROM orders
WHERE order_time BETWEEN '2022-07-01' AND '2022-07-07';
---------------------------------------------------------
LIKE

SELECT * FROM customers
WHERE first_name LIKE '%o%';                              - all names with o in it

WHERE first_name LIKE '_o_';                              - all names where only second letter is o, with only one character before and after

WHERE prize LIKE '3%';                                    - after 3. its any number
---------------------------------------------------------
ORDER BY

SELECT * FROM products
ORDER BY price ASC;                                       - ASC - ascending = from lowest price to highest price

ORDER BY price DESC;                                      - DESC - decending

SELECT * FROM products
WHERE customer_id = 1
ORDER BY price ASC;
---------------------------------------------------------

Q. From the products table, select the name and prize of all products with a coffee origin equal to columbia or indonesia. ordered by name from A-Z?

Ans.
SELECT name,prize FROM products
WHERE coffee_origin IN ('columbia','indonesia')
ORDER BY name;

Q. From the orders table, select all the orders from february 2017 for customers with id of 2,4,6,8?

Ans.
SELECT * FROM orders
WHERE orders_time BETWEEN '2017-02-01' AND '2017-02-28'
AND customer_id IN (2,4,6,8);

Q. From the customers table, select the first name and phone numbers of all the customers whose last name contains the pattern ar?

Ans.
SELECT first_name,phone_numbers,last_name FROM customers
WHERE last_name LIKE '%ar%';

--------------------------------------------------------------------------------------------------------------------------------------------
DISTINCT

SELECT DISTINCT customer_id FROM orders;                                                - Will give one output of multiple same name
---------------------------------------------------------
LIMIT

SELECT * FROM customers
LIMIT 5;                                                                                - will return 5 rows of data or 1-5

LIMIT 5 OFFSET 5;                                                                       - will give values from 6-10, next 5   

SELECT * FROM customers
ORDER BY last_name
LIMIT 5; 
---------------------------------------------------------
COLUMN NAME ALIAS

SELECT name AS coffee_price, coffee_origin AS country FROM products;                    - will change the column name of 'name' as 'coffee-price' and 
                                                                                          'coffee_origin" as 'country'
---------------------------------------------------------
Q. From the customers table, select distinct last names and order alphabetically from a-z?

Ans.
SELECT DISTINCT last_name FROM customers
ORDER BY last_name;

Q. From the orders table, select the first three orders placed by customer with id 1, in february 2017?

Ans.
SELECT * FROM orders
WHERE order_time BETWEEN '2017-07-01' AND '2017-07-28'
AND customer_id = 1
ORDER BY order_time ASC
LIMIT 3;

Q. From the products table, select the name price coffee origin but rename the price to retail price in the results set?

Ans.
SELECT name,price AS retail_price,coffee_origin FROM products;
--------------------------------------------------------------------------------------------------------------------------------------------

17. JOIN

Joins allows you to retrieve data from multiple tables into a single select statement.
To join two table we need a related column between them.
There are many different kinds of joins.
---------------------------------------------------------

INNER JOINS

SELECT names FROM orders
INNER JOIN products ON order.product_id = product.id;                                           - INNER JOIN <tablename> ON <table name>.<columnname> = 
                                                                                                  <table name>.<columnname> 

SELECT p.names,o.order_time FROM orders o
JOIN products p ON o.product_id = p.id                                                          - o resemble orders, p resemble products table
WHERE product_id = 5
ORDER BY o.order_time;
---------------------------------------------------------

LEFT JOIN - it retrieve data from 1st table - here order is 1st and cus is 2nd.

SELECT * FROM customers;
SELECT * FROM orders;

UPDATE orders
SET customer_id = 1
WHERE id = 1

SELECT o.id,c.phone_number,c.last_name,o.order_time FROM orders o
LEFT JOIN customers c ON o.customer_id = c.id
ORDER BY o.order_time
LIMIT 10;
---------------------------------------------------------
RIGHT JOIN - it retrieve data from 2nd table - here order is 1st and cus is 2nd.

SELECT * FROM customers;
SELECT * FROM orders;

UPDATE orders
SET customer_id = 1
WHERE id = 1

SELECT o.id,c.phone_number,c.last_name,o.order_time FROM orders o
RIGHT JOIN customers c ON o.customer_id = c.id
ORDER BY o.order_time
LIMIT 10;
---------------------------------------------------------
JOIN MULTIPLE TABLE

SELECT * FROM products;
SELECT * FROM customers;
SELECT * FROM orders;

SELECT p.name,p.price,c.first_name,c.last_name,o.order_time FROM products p
JOIN order o ON p.id = o.product_id
JOIN customers c ON c.id = o.customer_id
WHERE c.last_name = 'martin'
ORDER BY o.order_time;
---------------------------------------------------------
Q. Select the order id and customer phone number for all orders of product id 4.

Ans.
SELECT o.order_id,o.phone_number FROM orders o 
JOIN customer c ON o.customer_id = c_id
WHERE o.product_id = 4;

Q. Select product name and order time for filter coffee sold between jan 15th 2017 and feb 14th 2017.

Ans.
SELECT p.name,o.order_time FROM products p
JOIN orders o ON p.id = o.product_id
WHERE p.name = 'Filter'
AND o.order_time BETWEEN '2017-01-15' AND '2017-02-14'

Q. Select the product name and price and order time for all the orders from females in jan 2017.

Ans.
SELECT p.name,o.order_time,p.price FROM products p 
JOIN orders o ON p.id = o.product_id
JOIN customer c ON o.customer_id = c_id
WHERE c.gender = 'F'
AND o.order_time BETWEEN '2017-01-01' AND '2017-01-31';
--------------------------------------------------------------------------------------------------------------------------------------------
18.
NORMALIZATION

It is the process of efficienttly organizing data in a database.

Benefits:
1. Reduce storage space
2. Reduce delete, update and insert anomalies. eg.same data in different tables
3. Improve query performance

Level of normalization:
1. 1 normal form 1NF
2. 2 normal form 2NF
3. 3 normal form 3NF
4. Boyce and codd normal form BCNF
---------------------------------------------------------
1st normal form:

A table is in 1NF if:
> no repeated rows of data
> column only contains a single value
> The table has a primary key
---------------------------------------------------------
2nd normal form:

A table is in 2NF if:
> They confirm to 1 NF
> Every column that is not a primary key of the primary key is dependent on the whole of the primary key.

---------------------------------------------------------
3rd normal form:

A table is in 3NF if:
> They confirm to 2 NF
> Every column that is not a primary key of the primary key is dependent on the whole of the primary key.
--------------------------------------------------------------------------------------------------------------------------------------------
19. ReLATIONSHIP:
It means the links between the tables of that database.Tables are related through primary and foreign keys/columns.

Types:
one to one relationship:-
key in one table also appears only one time in second table.

one to many relationship:-
When a primary key of one table can be in multiple rows of foreign key column of another table.

many to mant relationship:-
when two tables have many instances of each other.
---------------------------------------------------------
Constraints:
NOT NULL: A column cant contain any null value.

UNIQUE: A column cant contain any duplicate value of data.

PRIMARY KEY: A column that uniquely identifies each row of data.

FOREIGN KEY: A column which is related to primary key in another table.

CHECK: Controls the values that can be inserted into a column. CHECK(rating between o AND 100)

DEFAULT: If no values is inserted into a column, you can set a default value. email DEFAULT 'no data'
--------------------------------------------------------------------------------------------------------------------------------------------
20. Create films table

[Syntax:id INT
        name VARCHAR(45)
        length_min INT]

SHOW DATABASES;

CREATE DATABASE cinema_system;

USE cinema_system;

CREATE TABLE films(
      id INT PRIMARY KEY AUTO_INCREMENT,
      name VARCHAR(45) NOT NULL UNIQUE,
      length_min INT NOT NULL
)
SHOW TABLES;

SELECT * FROM films;

DESCRIBE films;
---------------------------------------------------------
Create customers table

[Syntax:id INT
        first name VARCHAR(45)
        last name VARCHAR(45)
        email VARCHAR(45)]

SHOW DATABASES;

CREATE DATABASE cinema_system;

USE cinema_system;

CREATE TABLE customers(
      id INT PRIMARY KEY AUTO_INCREMENT,
      first name VARCHAR(45), 
      last name VARCHAR(45) NOT NULL,
      email VARCHAR(45) NOT NULL UNIQUE,
)
SHOW TABLES;

DESCRIBE customers;
---------------------------------------------------------
Create rooms table

[Syntax:id INT
        name VARCHAR(45)
        no_seats INT]

SHOW DATABASES;

CREATE DATABASE cinema_system;

USE cinema_system;

CREATE TABLE customers(
      id INT PRIMARY KEY AUTO_INCREMENT,
      first name VARCHAR(45), 
      last name VARCHAR(45) NOT NULL,
      email VARCHAR(45) NOT NULL UNIQUE,
)
SHOW TABLES;

DESCRIBE customers;